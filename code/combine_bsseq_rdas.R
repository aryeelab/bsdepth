library(bsseq)
library(Biobase)


#infiles <- "output/rdas_5X/adult_sorted_CD4_BioSam_327_draw_UW_RO_01721_5X_smooth_70_1000.rda output/rdas_5X/adult_sorted_CD4_BioSam_328_draw_UW_RO_01736_5X_smooth_70_1000.rda output/rdas_5X/adult_sorted_CD8_BioSam_329_draw_UW_RO_01721_5X_smooth_70_1000.rda output/rdas_5X/adult_sorted_CD8_BioSam_330_draw_UW_RO_01736_5X_smooth_70_1000.rda output/rdas_5X/BioSam_246_hippocampus_middle_5X_smooth_70_1000.rda output/rdas_5X/hippocampus_middle_BioSam_292_5X_smooth_70_1000.rda output/rdas_5X/liver_BioSam_161_REMC2_5X_smooth_70_1000.rda output/rdas_5X/liver_BioSam_162_REMC3_null_5X_smooth_70_1000.rda"
#outfile <- "output/rdas_5X/bs_5X_smooth_70_1000.rda"

infiles <- Sys.getenv("INFILES")
infiles <- unlist(strsplit(infiles, " "))
outfile <- Sys.getenv("OUTFILE")

message("Reading bsseq objects from:\n", paste(infiles, collapse="\n"))
bssList <- lapply(infiles, function(file) {
    load(file)
    bss
})

bss <- Reduce(combine, bssList)

# Record smoothing parameters
bss@parameters <- bssList[[1]]@parameters

# Add colors for plotting
pData <- pData(bss)
pData$col <- as.numeric(factor(pData$type))
pData(bss) <- pData

message("Saving bss to ", outfile)
save(bss, file=outfile)
message("Done.")

