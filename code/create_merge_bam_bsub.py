#!/usr/bin/env python
import os
import re

# use  Java-1.6; 

merged_bam_dir = "/seq/epigenome01/Martin/projects/bsdepth/merged_bams"
MERGE_SAMS = "java -XX:MaxPermSize=4g -XX:GCTimeLimit=50 -XX:GCHeapFreeLimit=10 -Xmx4000m -jar /seq/software/picard/1.96/bin/MergeSamFiles.jar"

# Read in initial sample description file (with bam names etc.)
f = open("input/SampleList_initial.txt", 'r')
of = open("code/merge_bam_bsub.sh", 'w')
f.readline() # skip header
sample_bams = {}
local_to_remote_map = {}
for line in f.readlines():
    fields = line.split("\t")
    sample_id =  fields[3].rstrip("_")
    bams = fields[14].rstrip(";").split(";")
    merged_bam = merged_bam_dir + "/" + sample_id + ".bam"
    cmd = "bsub -P epigenome -R 'rusage[mem=5000]' -q week -o " + merged_bam_dir + "/" + sample_id + ".mergelog '" + MERGE_SAMS + " INPUT=" + " INPUT=".join(bams) + " MERGE_SEQUENCE_DICTIONARIES=true OUTPUT=" + merged_bam + "'\n"
    of.write(cmd)

of.close()

# Read in replicate sample description file (with bam names etc.)
f = open("input/ReplicateSamples.txt", 'r')
of = open("code/merge_bam_bsub_2.sh", 'w')
f.readline() # skip header
#sample_bams = {}
#local_to_remote_map = {}
for line in f.readlines():
    fields = line.strip().split("\t")
    sample_id =  fields[0]
    bams = fields[2].rstrip(";").split(";")
    bams = [x.strip() for x in bams]
    merged_bam = merged_bam_dir + "/" + sample_id + ".bam"
    cmd = "bsub -P epigenome -R 'rusage[mem=5000]' -q week -o " + merged_bam_dir + "/" + sample_id + ".mergelog '" + MERGE_SAMS + " INPUT=" + " INPUT=".join(bams) + " MERGE_SEQUENCE_DICTIONARIES=true OUTPUT=" + merged_bam + "'\n"
    of.write(cmd)

of.close()

# Read in revision sample description file
f = open("input/SampleList_v2.txt", 'r')
of = open("code/merge_bam_bsub_3.sh", 'w')
f.readline() # skip header
#sample_bams = {}
#local_to_remote_map = {}
for line in f.readlines():
    fields = line.strip().split("\t")
    sample_id =  fields[0]
    bams = fields[1].rstrip(";").split(";")
    bams = [x.strip() for x in bams]
    merged_bam = merged_bam_dir + "/" + sample_id + ".bam"
    cmd = "bsub -P epigenome -R 'rusage[mem=5000]' -q week -o " + merged_bam_dir + "/" + sample_id + ".mergelog '" + MERGE_SAMS + " INPUT=" + " INPUT=".join(bams) + " MERGE_SEQUENCE_DICTIONARIES=true OUTPUT=" + merged_bam + "'\n"
    of.write(cmd)

of.close()

    
    