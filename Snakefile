# snakemake -j 500 -c "bsub {params.cluster}"

# Merge bams
# Erisone: python code/create_merge_bam_bsub.py  # Creates code/merge_bam_bsub.sh 
# Erisone: scp code/merge_bam_bsub.sh aryee@tin.broadinstitute.org:/seq/epigenome01/Martin/projects/bsdepth/merged_bams
# Erisone: scp code/merge_bam_bsub_2.sh aryee@tin.broadinstitute.org:/seq/epigenome01/Martin/projects/bsdepth/merged_bams
# Erisone: scp code/merge_bam_bsub_3.sh aryee@tin.broadinstitute.org:/seq/epigenome01/Martin/projects/bsdepth/merged_bams
# Broad: merge_bam_bsub.sh 
# Broad: merge_bam_bsub_2.sh 
# Broad: merge_bam_bsub_3.sh 
# Broad: for i in *.bam; do echo $i | tee -a ~/bs_flagstat.txt; samtools flagstat $i | tee -a ~/bs_flagstat.txt; done
# Broad: for i in *.bam; do bsub -P epigenome -q week -o $i.md5 "md5sum $i"; done

REF = "/data/aryee/pub/genomes/human_g1k_v37/human_g1k_v37.fasta"
BOWTIE = "module load bowtie-0.12.7; bowtie"
SAMTOOLS = "module load samtools-0.1.18; samtools"
PICARD_DIR = "/apps/source/picard-tools-1.84/picard-tools-1.84/"
COVERAGE_LEVELS = [30, 20, 10, 5, 2, 1]
#COVERAGE_LEVELS = [30]
CHRS = [str(chr) for chr in range(1,23)] + ['X', 'Y']
#CHRS = [21, 22]

import os
import re

# Set up symlinks to data directory:
if not os.path.exists("input"):
	os.symlink("/data/aryee/bsdepth/input", "input")
if not os.path.exists("output"):
	os.symlink("/data/aryee/bsdepth/output", "output")

sample_ids_tcell = ['cd41', 'cd42', 'cd81', 'cd82']
sample_ids_cd4 = ['cd41', 'cd42']
sample_ids_cd8 = ['cd81', 'cd82']
#sample_ids_hippocampus_liver = ['BioSam_246_hippocampus_middle', 'hippocampus_middle_BioSam_292', 'liver_BioSam_161_REMC2', 'liver_BioSam_162_REMC3_null']
sample_ids_brain = ['cortex1', 'cortex2', 'cortex3']
sample_ids_es = ['hES1', 'hES2', 'hES3']
sample_ids_brain_es = sample_ids_brain + sample_ids_es
sample_ids_brain2 = ['cortex1_cortex2', 'cortex1_cortex3', 'cortex2_cortex3']
sample_ids_es2 = ['hES1_hES2', 'hES1_hES3', 'hES2_hES3']
sample_ids_cd184 = ['cd1841', 'cd1842']
sample_ids_liver = ['liver1', 'liver2']
sample_ids_liver_cd184 = sample_ids_liver + sample_ids_cd184

sample_ids = sample_ids_tcell + sample_ids_brain_es + sample_ids_liver_cd184

localrules: all

rule all:
	input: "output/numreads",
		   # Brain vs ES, mincov=1, q=0.01
		  # expand("output/dmrtabs/dmrs_brain_es_1rep_mincov1/dmrs_brain_es_{coverage}X_smoothn70_smoothbp1000_mincov1_qcutoff0.01_grp1_{grp1}_vs_grp2_{grp2}_minloci0_minlength0_mindiff0.txt", coverage=COVERAGE_LEVELS, grp1=sample_ids_brain, grp2=sample_ids_es),
		  # expand("output/dmrtabs/dmrs_brain_es_2rep_mincov1/dmrs_brain_es_{coverage}X_smoothn70_smoothbp1000_mincov1_qcutoff0.01_grp1_{grp1}_vs_grp2_{grp2}_minloci0_minlength0_mindiff0.txt", coverage=COVERAGE_LEVELS, grp1=sample_ids_brain2, grp2=sample_ids_es2),
		  # expand("output/dmrtabs/dmrs_brain_es_3rep_mincov1/dmrs_brain_es_{coverage}X_smoothn70_smoothbp1000_mincov1_qcutoff0.01_grp1_cortex1_cortex2_cortex3_vs_grp2_hES1_hES2_hES3_minloci0_minlength0_mindiff0.txt", coverage=COVERAGE_LEVELS),
		   # CD4 vs CD8, mincov=1, q=0.01
		   expand("output/dmrtabs/dmrs_cd4_cd8_1rep_mincov1_qcutoff0.01/dmrs_tcell_{coverage}X_smoothn70_smoothbp1000_mincov1_qcutoff0.01_grp1_{grp1}_vs_grp2_{grp2}_minloci0_minlength0_mindiff0.txt", coverage=COVERAGE_LEVELS, grp1=sample_ids_cd4, grp2=sample_ids_cd8),
		   expand("output/dmrtabs/dmrs_cd4_cd8_2rep_mincov1_qcutoff0.01/dmrs_tcell_{coverage}X_smoothn70_smoothbp1000_mincov1_qcutoff0.01_grp1_cd41_cd42_vs_grp2_cd81_cd82_minloci0_minlength0_mindiff0.txt", coverage=COVERAGE_LEVELS),		   
		   # CD4 vs CD8, mincov=1, q=0.001
		   expand("output/dmrtabs/dmrs_cd4_cd8_1rep_mincov1_qcutoff0.001/dmrs_tcell_{coverage}X_smoothn70_smoothbp1000_mincov1_qcutoff0.001_grp1_{grp1}_vs_grp2_{grp2}_minloci0_minlength0_mindiff0.txt", coverage=COVERAGE_LEVELS, grp1=sample_ids_cd4, grp2=sample_ids_cd8),
		   expand("output/dmrtabs/dmrs_cd4_cd8_2rep_mincov1_qcutoff0.001/dmrs_tcell_{coverage}X_smoothn70_smoothbp1000_mincov1_qcutoff0.001_grp1_cd41_cd42_vs_grp2_cd81_cd82_minloci0_minlength0_mindiff0.txt", coverage=COVERAGE_LEVELS),		   
           # Liver vs cd184, q=0.01
		   expand("output/dmrtabs/dmrs_liver_cd184_1rep_mincov1_qcutoff0.01/dmrs_liver_cd184_{coverage}X_smoothn70_smoothbp1000_mincov1_qcutoff0.01_grp1_{grp1}_vs_grp2_{grp2}_minloci0_minlength0_mindiff0.txt", coverage=COVERAGE_LEVELS, grp1=sample_ids_liver, grp2=sample_ids_cd184),
		   expand("output/dmrtabs/dmrs_liver_cd184_2rep_mincov1_qcutoff0.01/dmrs_liver_cd184_{coverage}X_smoothn70_smoothbp1000_mincov1_qcutoff0.01_grp1_liver1_liver2_vs_grp2_cd1841_cd1842_minloci0_minlength0_mindiff0.txt", coverage=COVERAGE_LEVELS) 


# Brain vs ES, mincov=2, q=0.01
# expand("output/dmrtabs/dmrs_brain_es_1rep_mincov2/dmrs_brain_es_{coverage}X_smoothn70_smoothbp1000_mincov2_qcutoff0.01_grp1_{grp1}_vs_grp2_{grp2}_minloci0_minlength0_mindiff0.txt", coverage=COVERAGE_LEVELS, grp1=sample_ids_brain, grp2=sample_ids_es),
# expand("output/dmrtabs/dmrs_brain_es_2rep_mincov2/dmrs_brain_es_{coverage}X_smoothn70_smoothbp1000_mincov2_qcutoff0.01_grp1_{grp1}_vs_grp2_{grp2}_minloci0_minlength0_mindiff0.txt", coverage=COVERAGE_LEVELS, grp1=sample_ids_brain2, grp2=sample_ids_es2),
# expand("output/dmrtabs/dmrs_brain_es_3rep_mincov2/dmrs_brain_es_{coverage}X_smoothn70_smoothbp1000_mincov2_qcutoff0.01_grp1_cortex1_cortex2_cortex3_vs_grp2_hES1_hES2_hES3_minloci0_minlength0_mindiff0.txt", coverage=COVERAGE_LEVELS),
# CD4 vs CD8, mincov=2, q=0.01
# expand("output/dmrtabs/dmrs_cd4_cd82rep_mincov2/dmrs_tcell_{coverage}X_smoothn70_smoothbp1000_mincov2_qcutoff0.01_grp1_cd41_cd42_vs_grp2_cd81_cd82_minloci0_minlength0_mindiff0.txt", coverage=COVERAGE_LEVELS)
#expand("output/rdas_{coverage}X/bs_tcell_{coverage}X_smoothn70_smoothbp1000.rda", coverage=COVERAGE_LEVELS),
#expand("output/dmrtabs/dmrs_brain_es_{coverage}X_smoothn70_smoothbp1000_mincov2_qcutoff0.01_grp1_cortex1_cortex2_vs_grp2_hES1_hES3_minloci0_minlength0_mindiff0.txt", coverage=COVERAGE_LEVELS),
#expand("output/dmrtabs/dmrs_brain_es_{coverage}X_smoothn70_smoothbp1000_mincov2_qcutoff0.01_1reps_minloci0_minlength0_mindiff0.txt", coverage=COVERAGE_LEVELS),
#expand("output/dmrtabs/dmrs_brain_es_{coverage}X_smoothn70_smoothbp1000_mincov2_qcutoff0.01_2reps_minloci0_minlength0_mindiff0.txt", coverage=COVERAGE_LEVELS),
#expand("output/dmrtabs/dmrs_brain_es_{coverage}X_smoothn70_smoothbp1000_mincov2_qcutoff0.01_3reps_minloci0_minlength0_mindiff0.txt", coverage=COVERAGE_LEVELS),
#expand("output/dmrtabs/dmrs_tcell_{coverage}X_smoothn70_smoothbp1000_mincov2_qcutoff0.01_minloci1_minlength1_mindiff0.1.txt", coverage=COVERAGE_LEVELS),
#expand("output/dmrtabs/dmrs_tcell_{coverage}X_smoothn70_smoothbp1000_mincov2_qcutoff0.01_minloci2_minlength1_mindiff0.1.txt", coverage=COVERAGE_LEVELS),
#expand("output/dmrtabs/dmrs_tcell_{coverage}X_smoothn70_smoothbp1000_mincov2_qcutoff0.01_minloci3_minlength1_mindiff0.1.txt", coverage=COVERAGE_LEVELS)
#expand("output/rdas_{coverage}X/dmrs_brain_es_{coverage}X_smoothn70_smoothbp1000_mincov2_qcutoff0.01_1reps.rda", coverage=COVERAGE_LEVELS),
#expand("output/rdas_{coverage}X/dmrs_brain_es_{coverage}X_smoothn70_smoothbp1000_mincov2_qcutoff0.01_2reps.rda", coverage=COVERAGE_LEVELS),
#expand("output/rdas_{coverage}X/dmrs_brain_es_{coverage}X_smoothn70_smoothbp1000_mincov2_qcutoff0.01_3reps.rda", coverage=COVERAGE_LEVELS),
#expand("output/rdas_{coverage}X/bs_{coverage}X_smoothn500_smoothbp2000.rda", coverage=COVERAGE_LEVELS),
#"output/reports/dmrs_tcell_smoothn70_smoothbp1000_mincov2_qcutoff0.025_minloci3_mindiff0.1_minlength0.html",
#"output/reports/dmrs_tcell_smoothn70_smoothbp1000_mincov2_qcutoff0.01_minloci3_mindiff0.1_minlength0.html",
#"output/reports/dmrs_tcell_smoothn70_smoothbp1000_mincov2_qcutoff0.01_minloci5_mindiff0.1_minlength0.html"

#"output/dmrs_5X/dmrs_tcell_5X_smoothn70_smoothbp1000_mincov2_qcutoff0.025_minloci3_mindiff0.1.txt",
#expand("output/bam_{coverage}X/{sample}_{coverage}X.bam", coverage=COVERAGE_LEVELS, sample=sample_ids),
#expand("output/bam_{coverage}X/{sample}_{coverage}X.bam.numreads", coverage=COVERAGE_LEVELS, sample=sample_ids),
#"output/rdas_1X/bs_1X_smooth_70_1000_300.rda",
#expand("output/cpgtab_{coverage}X/{sample}_{coverage}X.cpg", coverage=COVERAGE_LEVELS, sample=sample_ids),
#expand("output/fastq/{sample}.{r}.fastq.gz", sample=sample_ids, r=[1,2]),
#expand("output/bam/{sample}.bam", sample=sample_ids),
#"output/bam_1X/adult_sorted_CD4_BioSam_328_draw_UW_RO_01736_1X.bam",
#"output/cpgtab_1X/adult_sorted_CD4_BioSam_328_draw_UW_RO_01736_1X.cpg",
#expand("output/rdas_1X/{sample}_1X_smooth_70_1000_300.rda", sample=sample_ids),
#"output/rdas_1X/adult_sorted_CD4_BioSam_328_draw_UW_RO_01736_1X_smooth_70_1000_300.rda",
#expand("output/rdas_{coverage}X/{sample}_{coverage}X.rda", coverage=COVERAGE_LEVELS, sample=sample_ids)


rule get_broad_bams:
	output: "output/broad_bam/{sample}.bam"
	params: broad_file = "/seq/epigenome01/Martin/projects/bsdepth/merged_bams/{sample}.bam",
			cluster = "-q medium"
	shell: "rsync -e ssh --progress aryee@copper.broadinstitute.org:{params.broad_file} {output}"

rule bam_to_fastq:
	output: r1="output/fastq/{sample}.1.fastq", r2="output/fastq/{sample}.2.fastq"
	input: "output/broad_bam/{sample}.bam"
	params: cluster = "-q medium"
	shell: "java -Xmx16g -jar {PICARD_DIR}/SamToFastq.jar INPUT={input} FASTQ={output.r1} SECOND_END_FASTQ={output.r2}"

rule gzip_fastq:
	output: "output/fastq/{sample}.fastq.gz"
	input: "output/fastq/{sample}.fastq"
	params: cluster = "-q medium"
	shell: "gzip {input}"

rule flagstat:
	output: "{sample}.bam.flagstat"
	input: "{sample}.bam"
	params: cluster = "-q medium"
	shell: "{SAMTOOLS} flagstat {input} > {output}"
	
rule num_reads:
	output: numreads="{sample}.bam.numreads"
	input: "{sample}.bam.flagstat"
	params: cluster = "-q medium"
	shell: """
		NUM_MAPPED=`cat {input} | grep "mapped (" | cut -f1 -d' '`	
		NUM_DUPS=`cat {input} | grep "duplicates" | cut -f1 -d' '`
		NUM_UNIQUE_MAPPED=`echo $NUM_MAPPED-$NUM_DUPS | bc`
		echo $NUM_UNIQUE_MAPPED > {output}
	"""

rule num_reads_summary:
	output: "output/numreads"
	input: expand("output/bam_{coverage}X/{sample}_{coverage}X.bam.numreads", coverage=COVERAGE_LEVELS, sample=sample_ids)
	params: cluster = "-q medium"
	shell: "for i in output/bam*/*.bam; do echo $i `cat $i.numreads` >> {output}; done"
	
rule bsmap:
	output: "output/bam/{sample}.bam"
	input: r1="output/fastq/{sample}.1.fastq.gz", r2="output/fastq/{sample}.2.fastq.gz"
	params: cluster = "-q big-multi -n 10 -R 'rusage[mem=16000]'"
	shell: """
		module load samtools-0.1.18
		module load aryee/bsmap-2.74
		bsmap -a {input.r1} -b {input.r2} -d {REF} -p 10 | samtools view -bS - > {output}
	"""

ruleorder: sort_bam_by_name > bsmap
	
rule sort_bam_by_name:
	output: "output/bam/{sample}.nsorted.bam"
	input: "output/bam/{sample}.bam"
	#params: prefix="output/bam/{sample}.nsorted", cluster = "-q big -R 'rusage[mem=30000]'"
	params: cluster = "-q big -R 'rusage[mem=30000]'"
	log: "logs/sort_bam_by_name.log"
	shell: """
		java -Xmx30g -jar {PICARD_DIR}/SortSam.jar INPUT={input} OUTPUT={output} SORT_ORDER=queryname TMP_DIR=/data/aryee/scratch/ MAX_RECORDS_IN_RAM=5000000
	"""
	
rule downsample:
		output: "output/bam_{coverage}X/{sample}_{coverage}X.bam"
		input: bam="output/bam/{sample}.bam", numreads="output/bam/{sample}.bam.numreads"
		params: cov="{coverage}", cluster = "-q big -R 'rusage[mem=40000]'"
		log: "log/downsample_{sample}.log"
		run: 
			gsize=0.80*3.02*10**9
			readlen=100
			coverage = int(params.cov)
			f = open(input.numreads, 'r')
			numreads = int(f.readline().strip())
			targetreads = int(coverage * gsize / readlen)
			prob = targetreads/numreads
			print ("Total reads = " + str(numreads))
			print ("Target reads = " + str(targetreads))
			print("Sampling fraction = " +  str(prob)) 
			#os.system("touch " + output[0])
			#cmd="java -XX:MaxPermSize=4g -XX:GCTimeLimit=50 -XX:GCHeapFreeLimit=10 -Xmx6000m -jar " + PICARD_DIR + "/DownsampleSam.jar INPUT=" + input.bam + " OUTPUT=" + output[0] + " PROBABILITY=" + str(prob)
			cmd="java -Xmx40000m -jar " + PICARD_DIR + "/DownsampleSam.jar INPUT=" + input.bam + " OUTPUT=" + output[0] + " PROBABILITY=" + str(prob)
			os.system(cmd)
	
rule c_table_by_chr:
	output: temp("output/cpgtab_{coverage}X/{sample}_{coverage}X_chr{chr}.c")
	input: "output/bam_{coverage}X/{sample}_{coverage}X.bam"
	params: chr="{chr}", cluster = "-q medium -R 'rusage[mem=5000]'"
	shell: """
		module load samtools-0.1.18
		module load python/2.7.5
		module load aryee/bsmap-2.74
		BSMAP=`which bsmap`
		BSMAPDIR=`dirname $BSMAP`
		python $BSMAPDIR/methratio.py -c {params.chr} -z --ref={REF} --remove-duplicate --combine-CpG --out={output} {input} 
	"""
	
rule cpg_table_by_chr:
	output: temp("{name}.cpg")
	input: "{name}.c"
	params: cluster = "-q medium"
	shell: """
		awk '(NR==1 || ($3=="+" && $4~/^..CG/ ))' {input} > {output}
	"""

rule cpg_table:
	output: "output/cpgtab_{coverage}X/{sample}_{coverage}X.cpg"
	input: "output/cpgtab_{{coverage}}X/{{sample}}_{{coverage}}X_chr{chr}.cpg".format(chr=chr) for chr in CHRS
	params: cluster = "-q medium"
	shell: """
		cat {input} | awk '!(NR>1 && $1=="chr")' > {output}
		"""
	
# This rule uses phenodata.txt to get sample name mappings
rule create_bsseq_rda:
	output: "output/rdas_{coverage}X/{sample}_{coverage}X.rda"
	input: "output/cpgtab_{coverage}X/{sample}_{coverage}X.cpg"
	params: cluster = "-q medium -R 'rusage[mem=8000]' -o output/rdas_{coverage}X/{sample}_{coverage}X.log"
	log: "output/rdas_{coverage}X/{sample}_{coverage}X.log"
	shell: """
		module load aryee/R-3.0.2 
		export CPGTAB="{input}"
		export RDA="{output}"
		Rscript code/create_bsseq_rda.R > {log}
	"""	

ruleorder: combine_bsseq_tcell > smooth_bsseq
ruleorder: combine_bsseq_brain_es > smooth_bsseq
ruleorder: combine_bsseq_liver_cd184 > smooth_bsseq


rule smooth_bsseq:
	output: "output/rdas_{coverage}X/{sample}_{coverage}X_smoothn{ns}_smoothbp{h}.rda"
	input: "output/rdas_{coverage}X/{sample}_{coverage}X.rda"
	params: ns="{ns}", h="{h}", cluster = "-q big -R 'rusage[mem=12000]' -o output/rdas_{coverage}X/{sample}_{coverage}X_smoothn{ns}_smoothbp{h}.log"
	log: "output/rdas_{coverage}X/{sample}_{coverage}X_smoothn{ns}_smoothbp{h}.log"
	shell: """
		module load aryee/R-3.0.2 
		export NS={params.ns}
		export H={params.h}
		export UNSMOOTH_RDA={input}
		export SMOOTH_RDA={output}
		Rscript code/smooth_bsseq_rda.R > {log}
	"""


rule combine_bsseq_tcell:
	output: "output/rdas_{coverage}X/bs_tcell_{coverage}X_smoothn{ns}_smoothbp{h}.rda"
	input: "output/rdas_{{coverage}}X/{sample}_{{coverage}}X_smoothn{{ns}}_smoothbp{{h}}.rda".format(sample=sample) for sample in sample_ids_tcell
	log: "output/rdas_{coverage}X/bs_{coverage}X_smoothn{ns}_smoothbp{h}.log"
	params: cluster = "-q big -M 196000 -R 'rusage[mem=196000]' -o output/rdas_{coverage}X/bs_tcell_{coverage}X_smoothn{ns}_smoothbp{h}.log"
	shell: """
		module load aryee/R-3.0.2 
		export INFILES="{input}"
		export OUTFILE="{output}"
		Rscript --vanilla code/combine_bsseq_rdas.R > {log}
	"""

rule combine_bsseq_brain_es:
	output: "output/rdas_{coverage}X/bs_brain_es_{coverage}X_smoothn{ns}_smoothbp{h}.rda"
	input: "output/rdas_{{coverage}}X/{sample}_{{coverage}}X_smoothn{{ns}}_smoothbp{{h}}.rda".format(sample=sample) for sample in sample_ids_brain_es
	log: "output/rdas_{coverage}X/bs_{coverage}X_smoothn{ns}_smoothbp{h}.log"
	params: cluster = "-q big -M 196000 -R 'rusage[mem=196000]' -o output/rdas_{coverage}X/bs_brain_es_{coverage}X_smoothn{ns}_smoothbp{h}.log"
	shell: """
		module load aryee/R-3.0.2 
		export INFILES="{input}"
		export OUTFILE="{output}"
		Rscript --vanilla code/combine_bsseq_rdas.R > {log}
	"""
	
rule combine_bsseq_liver_cd184:
	output: "output/rdas_{coverage}X/bs_liver_cd184_{coverage}X_smoothn{ns}_smoothbp{h}.rda"
	input: "output/rdas_{{coverage}}X/{sample}_{{coverage}}X_smoothn{{ns}}_smoothbp{{h}}.rda".format(sample=sample) for sample in sample_ids_liver_cd184
	log: "output/rdas_{coverage}X/bs_{coverage}X_smoothn{ns}_smoothbp{h}.log"
	params: cluster = "-q big -M 196000 -R 'rusage[mem=196000]' -o output/rdas_{coverage}X/bs_liver_cd184_{coverage}X_smoothn{ns}_smoothbp{h}.log"
	shell: """
		module load aryee/R-3.0.2 
		export INFILES="{input}"
		export OUTFILE="{output}"
		Rscript --vanilla code/combine_bsseq_rdas.R > {log}
	"""
	
#ruleorder: find_dmrs_tcell > smooth_bsseq
#ruleorder: find_dmrs_brain_es_numreps > smooth_bsseq 	
	
# rule find_dmrs_tcell:
# 	output: "output/rdas_{coverage}X/dmrs_tcell_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}.rda"
# 	input: "output/rdas_{coverage}X/bs_tcell_{coverage}X_smoothn{ns}_smoothbp{h}.rda"
# 	log: "output/rdas_{coverage}X/dmrs_tcell_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}.log"
# 	params: qcutoff="{qcutoff}", mincov="{mincov}", cluster = "-q big -R 'rusage[mem=16000]' -o output/rdas_{coverage}X/dmrs_tcell_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}.log"
# 	shell: """
# 		module load aryee/R-3.0.2 
# 		export GROUP1="cd41 cd42"
# 		export GROUP2="cd81 cd82"
# 		export MINCOV="{params.mincov}"
# 		export QCUTOFF="{params.qcutoff}"
# 		export BSSEQRDA="{input}"
# 		export DMRRDA="{output}"
# 		Rscript --vanilla code/find_dmrs.R > {log}
# 	"""


# rule find_dmrs_brain_es_numreps:
# 	output: "output/rdas_{coverage}X/dmrs_brain_es_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}_{numreps}reps.rda"
# 	input: "output/rdas_{coverage}X/bs_brain_es_{coverage}X_smoothn{ns}_smoothbp{h}.rda"
# 	log: "output/rdas_{coverage}X/dmrs_brain_es_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}.log"
# 	params: qcutoff="{qcutoff}", mincov="{mincov}", numreps="{numreps}", cluster = "-q big -R 'rusage[mem=16000]' -o output/rdas_{coverage}X/dmrs_brain_es_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}.log"
# 	shell: """
# 		module load aryee/R-3.0.2 
# 		export GROUP1="cortex_1 cortex_2 cortex_3"
# 		export GROUP2="hES_1 hES_2 hES_3"
# 		export NUMREPS="{params.numreps}"
# 		export MINCOV="{params.mincov}"
# 		export QCUTOFF="{params.qcutoff}"
# 		export BSSEQRDA="{input}"
# 		export DMRRDA="{output}"
# 		Rscript --vanilla code/find_dmrs_by_numreps.R > {log}
# 	"""

ruleorder: find_dmrs_brain_es_specified_samples > smooth_bsseq
rule find_dmrs_brain_es_specified_samples:
	output: "output/rdas_{coverage}X/dmrs_brain_es_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}_grp1_{group1}_vs_grp2_{group2}.rda"
	input: "output/rdas_{coverage}X/bs_brain_es_{coverage}X_smoothn{ns}_smoothbp{h}.rda"
	#log: "output/rdas_{coverage}X/dmrs_brain_es_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}.log"
	params: group1="{group1}", group2="{group2}", qcutoff="{qcutoff}", mincov="{mincov}", cluster = "-q big -R 'rusage[mem=16000]' -o output/rdas_{coverage}X/dmrs_brain_es_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}.log"
	shell: """
		module load aryee/R-3.0.2 
		export GROUP1="{params.group1}"
		export GROUP2="{params.group2}"
		export MINCOV="{params.mincov}"
		export QCUTOFF="{params.qcutoff}"
		export BSSEQRDA="{input}"
		export DMRRDA="{output}"
		Rscript --vanilla code/find_dmrs_for_specific_samples.R  # > {log}
	"""

ruleorder: find_dmrs_tcell_specified_samples > smooth_bsseq
rule find_dmrs_tcell_specified_samples:
	output: "output/rdas_{coverage}X/dmrs_tcell_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}_grp1_{group1}_vs_grp2_{group2}.rda"
	input: "output/rdas_{coverage}X/bs_tcell_{coverage}X_smoothn{ns}_smoothbp{h}.rda"
	#log: "output/rdas_{coverage}X/dmrs_tcell_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}.log"
	params: group1="{group1}", group2="{group2}", qcutoff="{qcutoff}", mincov="{mincov}", cluster = "-q big -R 'rusage[mem=16000]' -o output/rdas_{coverage}X/dmrs_tcell_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}.log"
	shell: """
		module load aryee/R-3.0.2 
		export GROUP1="{params.group1}"
		export GROUP2="{params.group2}"
		export MINCOV="{params.mincov}"
		export QCUTOFF="{params.qcutoff}"
		export BSSEQRDA="{input}"
		export DMRRDA="{output}"
		Rscript --vanilla code/find_dmrs_for_specific_samples.R  # > {log}
	"""

ruleorder: find_dmrs_liver_cd184_specified_samples > smooth_bsseq
rule find_dmrs_liver_cd184_specified_samples:
	output: "output/rdas_{coverage}X/dmrs_liver_cd184_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}_grp1_{group1}_vs_grp2_{group2}.rda"
	input: "output/rdas_{coverage}X/bs_liver_cd184_{coverage}X_smoothn{ns}_smoothbp{h}.rda"
	#log: "output/rdas_{coverage}X/dmrs_liver_cd184_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}.log"
	params: group1="{group1}", group2="{group2}", qcutoff="{qcutoff}", mincov="{mincov}", cluster = "-q big -R 'rusage[mem=16000]' -o output/rdas_{coverage}X/dmrs_liver_cd184_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}.log"
	shell: """
		module load aryee/R-3.0.2 
		export GROUP1="{params.group1}"
		export GROUP2="{params.group2}"
		export MINCOV="{params.mincov}"
		export QCUTOFF="{params.qcutoff}"
		export BSSEQRDA="{input}"
		export DMRRDA="{output}"
		Rscript --vanilla code/find_dmrs_for_specific_samples.R  # > {log}
	"""

#rule filter_dmrs_tcell:
#	output: dmrtab="output/dmrtabs/dmrs_tcell_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}_minloci{minloci}_minlength{minlength}_mindiff{mindiff}.txt", 
#			#dmrplots="output/dmrtabs/dmrs_tcell_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}_minloci{minloci}_minlength{minlength}_mindiff{mindiff}.pdf"
#	input: dmrs="output/rdas_{coverage}X/dmrs_tcell_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}.rda",
#		   bss="output/rdas_{coverage}X/bs_tcell_{coverage}X_smoothn{ns}_smoothbp{h}.rda"
#	log: "output/dmrs_{coverage}X/dmrs_tcell_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}_minloci{minloci}_minlength{minlength}_mindiff{mindiff}.log"
#	params: minloci="{minloci}", mindiff="{mindiff}", minlength="{minlength}", cluster="-q vshort"
#	shell: """
#			export MINLOCI="{params.minloci}"
#			export MINDIFF="{params.mindiff}"
#			export MINLENGTH="{params.minlength}"
#			export DMRRDA="{input.dmrs}"
#			export BSSEQRDA="{input.bss}"
#			export DMRTAB="{output.dmrtab}"
#			Rscript --vanilla code/filter_dmrs.R > {log}
#	"""

rule filter_dmrs:
	output: dmrtab="output/dmrtabs/{dmr_dir}/dmrs_{comparison}_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}_minloci{minloci}_minlength{minlength}_mindiff{mindiff}.txt", 
			#dmrplots="output/dmrtabs/dmrs_{comparison}_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}_minloci{minloci}_minlength{minlength}_mindiff{mindiff}.pdf"
	input: dmrs="output/rdas_{coverage}X/dmrs_{comparison}_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}.rda",
		   bss="output/rdas_{coverage}X/bs_{comparison}_{coverage}X_smoothn{ns}_smoothbp{h}.rda"
	log: "output/dmrs_{coverage}X/dmrs_{comparison}_{coverage}X_smoothn{ns}_smoothbp{h}_mincov{mincov}_qcutoff{qcutoff}_minloci{minloci}_minlength{minlength}_mindiff{mindiff}.log"
	params: minloci="{minloci}", mindiff="{mindiff}", minlength="{minlength}", cluster="-q vshort"
	shell: """
			module load aryee/R-3.0.2 
			export MINLOCI="{params.minloci}"
			export MINDIFF="{params.mindiff}"
			export MINLENGTH="{params.minlength}"
			export DMRRDA="{input.dmrs}"
			export BSSEQRDA="{input.bss}"
			export DMRTAB="{output.dmrtab}"
			Rscript --vanilla code/filter_dmrs.R > {log}
	"""



rule dmr_coverage_effect:
	output: "output/reports/dmr_coverage_effect.html"
	input: expand("output/dmrtabs/dmrs_tcell_{coverage}X_smoothn70_smoothbp1000_mincov2_qcutoff0.01_minloci3_minlength1_mindiff0.1.txt", coverage=COVERAGE_LEVELS)
	shell: """
		Rscript -e "require(knitr); rtext=readChar('code/dmr_coverage_effect.R', file.info('code/compare_dmrs.R')['size']); write(spin(text=rtext, knit=TRUE), file='{output}')"				
	"""

#rule compare_dmrs:
#	output: "output/reports/{label}_smoothn{params}.html"
#	input: "output/dmrs_{coverage}X/{{label}}_{coverage}X_smoothn{{params}}.txt".format(coverage=coverage) for coverage in COVERAGE_LEVELS
#	params: cluster="-q short"
#	shell: """
#		module load aryee/R-3.0.2 
#		export DMRFILES="{input}"
#		Rscript -e "require(knitr); rtext=readChar('code/compare_dmrs.R', file.info('code/compare_dmrs.R')['size']); write(spin(text=rtext, knit=TRUE), file='{output}')"				
#	"""

rule send_bs_to_enigma:
	shell: "scp  output/rdas_30X/bs_* maryee@enigma2.jhsph.edu:/home/bst/other/maryee/projects/bsdepth/rdas/"


